import React from 'react';
import Logo from '../../assets/logo.svg'

import { HeaderContainer } from './style';

function Header() {
  return (
      <HeaderContainer>
          <img src={Logo} alt="Logo"/>
      </HeaderContainer>
  );
}

export default Header;