import './App.css';

import React from 'react';
import Header from './components/Header';
import SideImg from './assets/side-image.png'
import Mask from './assets/mask.svg'

function App() {
  return (
    <section>
      <Header />
      <main>
        <div>
          <p>
          Seja bem-vinda(o) ao espaço da Renata, 
          aqui você encontra tudo de melhor para 
          seu <span>corpo</span> e sua <span>auto estima</span>
          </p>
          <button>CONHEÇA NOSSOS SERVIÇOS</button>
        </div>
        <img src={SideImg} alt="Imagem desenho"/>
      </main>
      <footer>
        <img src={Mask} alt="Mascara" />
        <p>Seguindo todos os protocolos contra a COVID-19</p>
      </footer>
    </section>
  );
}

export default App;
